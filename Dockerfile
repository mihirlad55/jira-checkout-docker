FROM python:windowsservercore-1809
MAINTAINER https://gitlab.com/mihirlad55/jira-checkout-docker

# Install required packages
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install jira pytz requests pyinstaller pycryptodome